<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 2</title>
    <style>
        body{
            height: 100vh;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
            background-color: beige;
            font-size: 2em;
        }
    </style>
</head>
<body>
    <h1>Page 2</h1>
    <p><a href="page1.php">page 1</a></p>
    <p><a href="page3.php">page 3</a></p>
    <?php
        echo "<p>$x</p>";
        $_SESSION['y'] = 13;
        $_SESSION['z'] = 14;
        echo "<p>".$_SESSION['y']."</p>";     
    ?>
</body>
</html>