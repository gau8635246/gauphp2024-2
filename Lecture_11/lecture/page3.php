<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 3</title>
    <style>
        body{
            height: 100vh;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
            background-color: beige;
            font-size: 2em;
        }
    </style>
</head>
<body>
    <h1>Page 3</h1>
    <p><a href="page1.php">page 1</a></p>
    <p><a href="page2.php">page 2</a></p>
    <p><a href="page4.php">page 4</a></p>
    <?php
        echo "<p>".$_SESSION['y']."</p>"; 
        echo "<p>".$_SESSION['z']."</p>"; 
        unset($_SESSION['z']); 
        $_SESSION['k'] = 15;
    ?>
</body>
</html>