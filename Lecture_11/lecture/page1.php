<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 1</title>
    <style>
        body{
            height: 100vh;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
            background-color: beige;
            font-size: 2em;
        }
    </style>
</head>
<body>
    <h1>Page 1</h1>
    <p><a href="page2.php">page 2</a></p>
    <?php
        $x = 12;
        echo "<p>$x</p>";    
    ?>
</body>
</html>