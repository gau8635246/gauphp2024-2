<?php
    require_once "config/conn.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-commerce</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
      <span> Header </span>
    </header>
    <main>
        <?php
            include "blocks/nav.php";
            include "blocks/products.php";
        ?>
    </main>
    <footer>@ 2024</footer>
</body>
</html>