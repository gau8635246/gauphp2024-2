<form method="post">
    <div>
        <input type="text" name="name" placeholder="Name"> <span><?=$name_error?></span>
    </div>
    <div>
        <input type="text" name="lastname" placeholder="Lastname"> <span><?=$lastname_error?></span>
    </div>
    <div>
        <input type="text" name="email" placeholder="E-Mail"> <span><?=$email_error?></span>
    </div>
    <div>
        <?php 
            foreach($subjects as $_subject){
        ?>
        <div>
            <input type="checkbox" name="subjects[]" value="<?=$_subject['subject']?>"> 
            - <label for=""> <?=$_subject['subject']?> (<?=$_subject['ects']?> ECTS)</label>
        </div>
        <?php
            }
        ?>
    </div>
    <div>
        <input type="submit" value="Registration Academically" name="student_reg">
    </div>
</form>