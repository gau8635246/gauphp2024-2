<?php
    include "q_for_quiz.php";
    // echo "<pre>";
    // print_r($questions);
    // echo "</pre>";
    // shuffle($questions);
?>
<link rel="stylesheet" href="quiz/style.css">
<div class="container">
    <form action="?nav=lecturer" method="post">
        <h2>ქვიზი</h2>
        <div class="row title">
            <div class="question">კითხვა</div>
            <div class="point">მაქს. ქულა</div>
            <div>პასუხი</div>
        </div>
        <?php
        foreach($questions as $item):         
        ?>
        <div class="row">
            <div class="question"><?=$item['question']?></div>
            <div class="point"><?=$item['point']?></div>
            <div><input type="text" placeholder="პასუხი" name="answer[]"></div>
        </div>
        <?php
            endforeach;
        ?>
        <div class="row">
            <div>
                <label for="">სტუდენტი: </label>
                <input type="text" placeholder="სახელი" name="name">
                <input type="text" placeholder="გვარი" name="lastname">
                <button>გაგზავნა</button>
            </div>
        </div>
    </form>
</div>  