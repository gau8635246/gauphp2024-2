<?php
     // print_r($_POST);
    // if($_POST)  if($_SERVER['REQUEST_METHOD']=="POST")
    $f_size = 5 * 1024 * 1024;
    $extensions = ['txt', 'csv'];
    $error_mes = "";
    $root_path = "upload_file/storage/";


    if(isset($_POST['upload_file'])){
        // echo "<pre>";
        // print_r($_FILES['f_name']);
        // echo "</pre>";

        $patch = $root_path.$_FILES['f_name']['name'];

        $f_type = pathinfo($patch);
        // echo "<pre>";
        // print_r($f_type);
        // echo "</pre>";
        
        
        if($_FILES['f_name']['size'] > $f_size || !in_array($f_type['extension'], $extensions)){
            $error_mes = "File size is over or file type is not valid!!";
        }else{
            move_uploaded_file($_FILES['f_name']['tmp_name'], $patch);

        }        
    }  


    if(isset($_GET['del'])){
        unlink($root_path.$_GET['del']);
        header("location: index.php");
    }
?>