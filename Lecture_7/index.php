<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 7</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
   <nav>
    <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="index.php?nav=quiz">Quiz</a></li>
        <li><a href="index.php?nav=subject">Subject</a></li>
        <li><a href="index.php?nav=fandf">File and Folder</a></li>
        <li><a href="index.php">Upload File</a></li>
    </ul>
   </nav>
   <div class="content">
        <?php
            if(isset($_GET['nav']) && $_GET['nav']=="quiz"){
                // echo "<h1>QUIZ</h1>";
                include "quiz/index.php";
            }else if(isset($_GET['nav']) && $_GET['nav']=="lecturer"){
                include "quiz/lecturer.php";
            }else if(isset($_GET['nav']) && $_GET['nav']=="subject"){
                echo "<h1>SUBJECT</h1>";
            }else if(isset($_GET['nav']) && $_GET['nav']=="fandf"){
                echo "<h1>File and Folder</h1>";
            }else{          
                include "upload_file/index.php";
            }
        ?>
   </div> 
</body>
</html>