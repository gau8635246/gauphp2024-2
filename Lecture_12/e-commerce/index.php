<?php
    session_start();
    require_once "config/conn.php";
    require_once "admin/user_permission.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-commerce</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
      <span> Header </span>
      <div class="sign">
        <?php
            if(!isset($_SESSION['user'])){        
        ?>
        <span><a href="?sign=in">Sign In</a></span> 
        <span><a href="?sign=up">Sign Up</a></span>
        <?php 
            }else{
        ?>
        <span><a href="?sign=out">Sign out</a></span> 
        <span><?=$_SESSION['user']?></span>
        <?php 
            }
        ?>
    </div>
    </header>
    <main>
        <?php
            if(isset($_SESSION['user'])){
                include "blocks/admin_nav.php";
            }else{
                include "blocks/nav.php";
            }
            if(isset($_GET['sign']) && $_GET['sign']=='in' && !isset($_SESSION['user'])){
                include "blocks/signin.php";
            }else if(isset($_SESSION['user'])){
                if(isset($_GET['main']) && $_GET['main']=='cat'){
                    include "blocks/admin/categorys.php";
                }
            }else{
                include "blocks/products.php";
            }
        ?>
    </main>
    <footer>@ 2024</footer>
</body>
</html>