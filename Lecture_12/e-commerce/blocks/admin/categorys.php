<?php 
    $query_cat = "SELECT * FROM categorys ORDER BY id DESC";
    $result_cat = mysqli_query($conn, $query_cat);

    if(isset($_POST['add_cat'])){
        $name = $_POST['name'];
        $description = $_POST['description'];
        mysqli_query($conn, "INSERT INTO categorys(name, description) VALUES ('$name', '$description')");
        $url = $_SERVER['REQUEST_URI'];
        header("location: $url");
    }

    if(isset($_GET['drop'])){
        $id = $_GET['drop'];
        mysqli_query($conn, "DELETE FROM categorys WHERE id='$id'");
        $url = $_SERVER['PHP_SELF']."?main=cat";
        header("location: $url");
    }


    // echo "<pre>";
    // print_r($_SERVER);
    // echo "</pre>";
?>
<div class="categorys">
    <div class="add">
        <a href="?main=cat&&crud=add">Add Category</a>
        <?php 
            if(isset($_GET['crud']) && $_GET['crud']=='add'){
                include "add.php";
            }else if(isset($_GET['edit'])){
                include "edit.php";
            }
        ?>
    </div>
    <table class="data">
        <tr>
            <th>ID</th>
            <th>სახელი</th>
            <th>აღწერა</th>
            <th>შექმნა</th>
            <th>შეცვლა</th>
            <th>რედაქტირება</th>
            <th>წაშლა</th>
        </tr>
        <?php 
            while($data_cat = mysqli_fetch_assoc($result_cat)){
        ?>
        <tr>
            <td><?=$data_cat["id"]?></td>
            <td><?=$data_cat["name"]?></td>
            <td><?=$data_cat["description"]?></td>
            <td><?=$data_cat["created_at"]?></td>
            <td><?=$data_cat["updated_at"]?></td>
            <td><a href="?main=cat&&edit=<?=$data_cat["id"]?>">Edit</a></td>
            <td><a href="?main=cat&&drop=<?=$data_cat["id"]?>">Delete</a></td>
        </tr>
        <?php
          }
        ?>
    </table>
</div>